<?php 
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Headers: Content-Type"); ?> <?php

require_once('settings.php');

$query = $_GET["query"];
$tweets = getTweetsWith($query);

$sentiment = array();
foreach ($tweets as &$tweet) {
	$result = $DatumboxAPI->SentimentAnalysis($tweet);
	if (!isset($sentiment[$result])) {
	    $sentiment[$result] = 0;
	}
	$sentiment[$result]++;
}

unset($DatumboxAPI);

print json_encode($sentiment);